
class Patient:
        
    def display(self):
        print(self.__id)
        print(self.__name)
        print(self.__ssn)
    
    def setID(self,id):
        self.__id = id
        
    def getID(self):
        return self.id
    
    def setName(self,name):
        self.__name = name
    
    def getName(self):
        return self.name
    
    def setSsn(self,ssn):
        self.__ssn = ssn
        
    def getSsn(self):
        return self.ssn
    
p = Patient()
p.setID(101)
p.setName("Eddie")
p.setSsn("12345-2211")
print(p._Patient__id)
print(p._Patient__name)
print(p._Patient__ssn)