from threading import Thread

class MyThread:
    
    def displayNumbers(self):
        i=0
        while(i<=10):
            print(i)
            i=i+1
            
obj = MyThread()
t = Thread(target=obj.displayNumbers())
t.start()