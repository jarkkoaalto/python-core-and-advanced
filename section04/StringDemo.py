s = "  Your are awesom   "
print(s)

s1 = """
You are 
the  creator
of you destiny
"""
print(s1)

print(s[2])

# Repetion
print(s*3)

#Lenght
print(len(s1))
print(len(s))

#Slicing

print(s[0:6])
print(s[:8])
print(s[-3:-1])


print(s[0:9:2])
print(s[0::3])

print(s.strip())
print(s.lstrip())
print(s.rstrip())

print(s.find("awe"),0,len(s))
print(s.count("a"))
print(s.replace('awesom','super'))

print(s.upper())
print(s.lower())
print(s.title())