import re

str="Take 1 up one  23idea. one idea 45 at the time"
result = re.search(r'o\w\w', str)
print(result.group())

result = re.findall(r'o\w\w', str)
print(result)

result = re.match(r'T\w\w',str)
print(result.group())

result = re.findall(r'0\w{1,2}', str)
print(result)

result = re.split(r'\d+',str)
print(result)