# Python Core and Advanced
About this course: Master the fundamentals of Python 3 in easy steps.
Course content:
### Section 1: Introduction
### Section 2: Software Setup and First Python Script
### Section 3: Datatypes
### Section 4: Sequence Types
### Section 5: Operators and Operands
### Section 6: Input and Output functions
### Section 7: More Programs
### Section 8: Flow Control Statements
### Section 9: Command line arguments
### Section 10: Functions
### Section 11: Lambdas
### Section 12: List Comprehensions
### Section 13: Object Oriented Programming
### Section 14: Encapsulation
### Section 15: Inheritance
### Section 16: Polymorphism
### Section 17: Abstraction
### Section 18: Exception Handling Assertions and Logging
### Section 19: Files
### Section 20: Regular Expressions
### Section 21: Date and Time
### Section 22: Threads
### Section 23: Networking