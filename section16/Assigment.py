from abc import abstractmethod
class TouchScreenLaptop():
    
    def __init_(self, scroll, click):  
        self.scroll = scroll
        self.click = click
    
    @abstractmethod 
    def scroll(self):
        pass
    @abstractmethod
    def click(self):
        pass
      
class HP(TouchScreenLaptop):
    
    def scroll(self):
        print("HP Scrolling Screen")
    
class Dell(TouchScreenLaptop):
        
    def scroll(self):
        print("DELL Scrolling Screen")
    
class HPNotebook(TouchScreenLaptop):
   
    def click(self):
        print("HPNotebook clicking")
        
class DELLNotebook(TouchScreenLaptop):
    
    def click(self):
        print("DELLNotebook clicking")
        
        
        
h = HP()   
h.scroll()
d = Dell()
d.scroll()
hn = HPNotebook()
hn.click()
dn  = DELLNotebook()
dn.click()       
        
        