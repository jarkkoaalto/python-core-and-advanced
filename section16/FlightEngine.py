class Flight:
    def __init__(self, engine):
        self.engine=engine
        
    def startEngine(self):
        self.engine.start()
        
class AirbusEngine:
    def start(self):
        print("Starting Airbus Engine")
        
class BoingEngine:
    def start(self):
        print("Starting Boing Engind")
        
ae = AirbusEngine()
f = Flight(ae)
f.startEngine()


be = BoingEngine()
l = Flight(be)
l.startEngine()