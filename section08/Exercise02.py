x = int(input("Enter a number :"))

while x < 1000:
    x = x + 1
    if x % 10 == 0:
        continue
    if x > 100:
        break
    print(x)