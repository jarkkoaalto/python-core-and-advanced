class ObjectCounter:
    numberOfObjects = 0
    
    def __init__(self):
        ObjectCounter.numberOfObjects += 1
        
    @staticmethod
    def displayCount():
        print(ObjectCounter.numberOfObjects)

o1 = ObjectCounter()
o1.displayCount()
o2 = ObjectCounter()
o2.displayCount()        