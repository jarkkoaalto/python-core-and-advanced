class Car:
    def __init__(self, make, year):
        self.make = make
        self.year = year
        
    class Engine:
        def __init__(self,number):
            self.number = number
        def start(self):
            print("engine started")    
    
c = Car("BMW",2018)
print(c.Engine(11).number)
c.start()
