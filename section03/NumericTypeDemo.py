a=13
b = 100
c = -66
print(a,b,c)
print(type(a))

x = 33.5
y = -25.8
z = 205
print(x,y,z)
print(type(z))

d = 3 + 5j
print(d)
print(type(d))

e = 0B1010
print(e)
print(type(e))

f=0XFF
print(f)
print(type(f))

# Boolean type
g = True
print(g)
print(type(g))
print(9<8)

# Convert
h = int(x)
print(h)
print(type(h))


i=float("22.5")
print(i)
print(type(i))

print(bin(100))
print(hex(100))
print(oct(100))

