class BMW:

    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        
    def start(self):
        print("Starting the car")
        
    def stop(self):
        print("Stopping the car")

class ThreeSeries(BMW):
    def __init__(self, cruiseControlEnabled, make, model, year):
        super().__init__(make, model, year)
        self.cruiseControlEnabled = cruiseControlEnabled
        
    def display(self):
        print(self.cruiseControlEnabled)
    
    def start(self):
        super().start()
        print("Button Start")
        
        
class FiveSeries(BMW):
    def __init__(self, parkingAssistantEnabled, make, model, year):
        super().__init__(make, model, year)
        self.parkingAssistantEnabled = parkingAssistantEnabled
        
        
ThreeSeries = ThreeSeries(True,"BMW", "328i","2008")
print(ThreeSeries.cruiseControlEnabled)
print(ThreeSeries.model)
print(ThreeSeries.year)
ThreeSeries.start()
ThreeSeries.stop()
ThreeSeries.display()



FiveSeries = FiveSeries(True,"BMW", "512", "2014")
print(FiveSeries.parkingAssistantEnabled)
print(FiveSeries.model)
print(FiveSeries.year)
FiveSeries.start()
FiveSeries.stop()